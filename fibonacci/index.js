var fibonacci = function(N) {
    var firstFibonacciNum = 1;
    var secondFibonacciNum = 1;
    var askedFibonacciNum;
    var fibonacciArray = [firstFibonacciNum, secondFibonacciNum];

    if (N === 1) return firstFibonacciNum;
    else if (N === 2) return secondFibonacciNum;
    else {
        for (var i = 2; i < N; i++) {
            var nextFibonacciNum = fibonacciArray[i-2]+fibonacciArray[i-1];
            fibonacciArray.push(nextFibonacciNum);
        }
        askedFibonacciNum = fibonacciArray[fibonacciArray.length-1];
        return askedFibonacciNum;
    }
}